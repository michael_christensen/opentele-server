databaseChangeLog = {

	changeSet(author: "toby (generated)", id: "1417695269597-1") {
		createTable(tableName: "help_image") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "help_imagePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "filename", type: "varchar(512)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "toby (generated)", id: "1417695269597-2") {
		createTable(tableName: "help_info") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "help_infoPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "help_image_id", type: "bigint")

			column(name: "text", type: "longvarchar")
		}
	}

	changeSet(author: "toby (generated)", id: "1417695269597-3") {
		createTable(tableName: "patient_help_info") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "patient_help_PK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "help_image_id", type: "bigint")

			column(name: "text", type: "longvarchar")
		}
	}

	changeSet(author: "toby (generated)", id: "1417528665697-4") {
		addColumn(tableName: "patient_questionnaire_node") {
			column(name: "help_info_id", type: "bigint")
		}
	}

	changeSet(author: "toby (generated)", id: "1417695269597-5") {
		addColumn(tableName: "questionnaire_node") {
			column(name: "help_info_id", type: "bigint")
		}
	}

	changeSet(author: "toby (generated)", id: "1417695269597-6") {
		addForeignKeyConstraint(baseColumnNames: "help_image_id", baseTableName: "help_info", constraintName: "FK4D4FAEECF165DE37", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "help_image", referencesUniqueColumn: "false")
	}

	changeSet(author: "toby (generated)", id: "1417695269597-7") {
		addForeignKeyConstraint(baseColumnNames: "help_info_id", baseTableName: "patient_questionnaire_node", constraintName: "FKC44D1298E4CDFFB2", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "patient_help_info", referencesUniqueColumn: "false")
	}

	changeSet(author: "toby (generated)", id: "1417695269597-8") {
		addForeignKeyConstraint(baseColumnNames: "help_info_id", baseTableName: "questionnaire_node", constraintName: "FK7BD3671E719CA8E8", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "help_info", referencesUniqueColumn: "false")
	}
}
