package org.opentele.server.model.patientquestionnaire

import org.opentele.server.model.HelpImage

class PatientHelpInfo {
    String text
    HelpImage helpImage

    static belongsTo = [questionnaireNode: PatientQuestionnaireNode]

    static mapping = {
        text type: 'text'
    }

    static constraints = {
        text(nullable: true, blank:true)
        helpImage(nullable: true)
    }
}
