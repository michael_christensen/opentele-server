package org.opentele.server.model

class HelpImage {
    String filename

    static constraints = {
        filename(blank:false,nullable:false)
    }
}
